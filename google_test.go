package main

import (
	"fmt"
	"strings"
	"testing"

	"github.com/harborapp/harbor-oauth/physical"
	"github.com/stretchr/testify/assert"
)

type mockGoogleStorer struct {
	throw bool
}

func (m mockGoogleStorer) StoreState(uuid string, values map[string]string) error {
	if m.throw {
		return fmt.Errorf("Storer error")
	}

	return nil
}

func (m mockGoogleStorer) GetState(uuid string) (map[string]string, error) {
	v := make(map[string]string)

	return v, nil
}

func TestLoginURL(t *testing.T) {
	tests := []struct {
		name        string
		clientID    string
		redirect    string
		identifier  string
		accessType  string
		scopes      []string
		state       map[string]string
		storer      physical.Storer
		expected    string
		expectedErr string
	}{
		{
			name:        "with no clientId",
			clientID:    "",
			redirect:    "localhost:8080",
			scopes:      []string{"view:email"},
			expectedErr: "Not enough parameters",
			storer:      mockGoogleStorer{},
		},
		{
			name:        "with no redirectURL",
			clientID:    "1234",
			redirect:    "",
			scopes:      []string{"view:email"},
			expectedErr: "Not enough parameters",
			storer:      mockGoogleStorer{},
		},
		{
			name:        "with no scopes",
			clientID:    "1234",
			redirect:    "localhost:8080",
			expected:    "",
			expectedErr: "Not enough parameters",
			storer:      mockGoogleStorer{},
		},
		{
			name:     "with only required values",
			clientID: "1234",
			redirect: "localhost:8080",
			scopes:   []string{"view:email", "view:profile"},
			expected: "?client_id=1234&redirect_uri=localhost%3A8080&response_type=code&scope=view%3Aemail+view%3Aprofile",
			storer:   mockGoogleStorer{},
		},
		{
			name:       "with access type",
			clientID:   "1234",
			redirect:   "localhost:8080",
			scopes:     []string{"view:email", "view:profile"},
			accessType: "offline",
			expected:   "?access_type=offline&client_id=1234&redirect_uri=localhost%3A8080&response_type=code&scope=view%3Aemail+view%3Aprofile",
			storer:     mockGoogleStorer{},
		},
		{
			name:       "with state",
			clientID:   "1234",
			redirect:   "localhost:8080",
			scopes:     []string{"view:email", "view:profile"},
			state:      map[string]string{"request_id": "1"},
			expected:   "?client_id=1234&redirect_uri=localhost%3A8080&response_type=code&scope=view%3Aemail+view%3Aprofile&state=request_id%3D1%26request_id%3D1234",
			identifier: "1234",
			storer:     mockGoogleStorer{},
		},
		{
			name:        "with bad storer",
			clientID:    "1234",
			redirect:    "localhost:8080",
			scopes:      []string{"view:email", "view:profile"},
			state:       map[string]string{"request_id": "1"},
			identifier:  "1234",
			storer:      mockGoogleStorer{throw: true},
			expectedErr: "Could not store state",
		},
		{
			name:        "with state but no identifier",
			clientID:    "1234",
			redirect:    "localhost:8080",
			scopes:      []string{"view:email", "view:profile"},
			state:       map[string]string{"request_id": "1"},
			storer:      mockGoogleStorer{},
			expectedErr: "State cannot be injected without a non-empty identifier",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			g := google{test.clientID, test.storer}
			url, err := g.LoginURL(test.redirect, test.accessType, test.scopes, test.state, test.identifier)
			if err != nil {
				assert.Equal(t, test.expectedErr, err.Error())
			} else {
				queries := strings.TrimPrefix(url, "https://accounts.google.com/o/oauth2/v2/auth")
				assert.Equal(t, test.expected, queries)
			}
		})
	}
}
