package main

import (
	"fmt"
	"net/url"
	"reflect"
	"strings"

	"github.com/harborapp/harbor-oauth/physical"
)

// google is a struct to help with OAuth tasks related to Google.
type google struct {
	clientID string
	store    physical.Storer
}

func (g *google) LoginURL(redirect, accessType string, scopes []string, state map[string]string, identifier string) (string, error) {
	const baseURL = "https://accounts.google.com/o/oauth2/v2/auth"
	u, _ := url.Parse(baseURL)

	q := u.Query()

	if g.clientID == "" || redirect == "" || len(scopes) == 0 {
		return "", fmt.Errorf("Not enough parameters")
	}

	// We want an authorization code in the callback URL.
	q.Add("response_type", "code")
	q.Add("client_id", g.clientID)
	q.Add("redirect_uri", redirect)
	q.Add("scope", g.joinScopes(scopes))

	if accessType != "" {
		q.Add("access_type", accessType)
	}

	// States are also optional.
	var states []string
	if len(state) != 0 {
		for k, v := range state {
			states = append(states, fmt.Sprintf("%s=%s", k, v))
		}

		if identifier == "" {
			return "", fmt.Errorf("State cannot be injected without a non-empty identifier")
		}

		// also append a unique uuid to identify the request.
		states = append(states, fmt.Sprintf("request_id=%s", identifier))

		q.Add("state", strings.Join(states, "&"))

		if err := g.store.StoreState(identifier, state); err != nil {
			return "", fmt.Errorf("Could not store state")
		}
	}

	u.RawQuery = q.Encode()

	return u.String(), nil
}

// VerifyCallback checks if the callback request is valid.
func (g *google) VerifyCallback(uid string, state map[string]string) (bool, error) {
	values, err := g.store.GetState(uid)
	if err != nil {
		return false, err
	}

	return reflect.DeepEqual(values, state), nil
}

// joinScopes serializes all required scopes into a single string
func (g *google) joinScopes(scopes []string) string {
	return strings.Join(scopes, " ")
}
