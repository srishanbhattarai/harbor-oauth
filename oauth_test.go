package main

import (
	"context"
	"fmt"
	"testing"

	oauth "github.com/harborapp/harbor-oauth/proto"
	"github.com/stretchr/testify/assert"
)

type mockStorer struct {
	getStateThrow bool
	getStateNil   bool
	getStateMap   map[string]string
}

func (m mockStorer) StoreState(uuid string, values map[string]string) error {
	return nil
}
func (m mockStorer) GetState(uuid string) (map[string]string, error) {
	if m.getStateThrow {
		return nil, fmt.Errorf("mock storer throw")
	}

	if m.getStateNil {
		return nil, nil
	}

	if m.getStateMap != nil {
		return m.getStateMap, nil
	}

	v := make(map[string]string)

	return v, nil
}

func TestSrvLoginURL(t *testing.T) {
	tests := []struct {
		name        string
		provider    int32
		redirect    string
		scopes      []string
		accessType  string
		state       map[string]string
		expected    string
		expectedErr string
	}{
		{
			name:        "when provider isn't supported",
			provider:    1,
			expectedErr: "Provider unsupported",
		},
		{
			name:     "when request is valid",
			provider: int32(oauth.OAuthProviders_GOOGLE),
			redirect: "localhost:8080",
			scopes:   []string{"view:email"},
			state:    map[string]string{"domain": "localhost"},
			expected: "https://accounts.google.com/o/oauth2/v2/auth?client_id=1234&redirect_uri=localhost%3A8080&response_type=code&scope=view%3Aemail&state=domain%3Dlocalhost%26",
		},
		{
			name:        "when request is missing parameters",
			provider:    int32(oauth.OAuthProviders_GOOGLE),
			scopes:      []string{"view:email"},
			state:       map[string]string{"domain": "localhost"},
			expectedErr: "Not enough parameters",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := &oauth.LoginURLReq{
				Provider:    oauth.OAuthProviders(test.provider),
				RedirectURL: test.redirect,
				Scopes:      test.scopes,
				AccessType:  test.accessType,
				State:       test.state,
			}
			res := &oauth.LoginURLRes{}
			srv := oauthService{&google{"1234", mockStorer{}}}

			err := srv.LoginURL(context.Background(), req, res)
			if err != nil {
				assert.Equal(t, test.expectedErr, err.Error())
			} else {
				assert.Contains(t, res.Url, test.expected)
			}
		})
	}
}

func TestSrvVerifyCallback(t *testing.T) {
	tests := []struct {
		name          string
		provider      int32
		state         map[string]string
		identifier    string
		getStateThrow bool
		getStateNil   bool
		getStateMap   map[string]string
		expected      bool
		expectedErr   string
	}{
		{
			name:        "when provider is not supported",
			provider:    1,
			expectedErr: "Provider unsupported",
		},
		{
			name:        "when state matches to be nil",
			provider:    int32(oauth.OAuthProviders_GOOGLE),
			state:       nil,
			getStateNil: true,
			expected:    true,
		},
		{
			name:        "when state matches to be a map",
			provider:    int32(oauth.OAuthProviders_GOOGLE),
			state:       map[string]string{"key": "value"},
			getStateMap: map[string]string{"key": "value"},
			expected:    true,
		},
		{
			name:        "when state doesn't match",
			provider:    int32(oauth.OAuthProviders_GOOGLE),
			state:       map[string]string{"key": "value"},
			getStateMap: map[string]string{"key1": "value1"},
			expected:    false,
		},
		{
			name:          "when storer throws",
			provider:      int32(oauth.OAuthProviders_GOOGLE),
			state:         map[string]string{"key": "value"},
			getStateThrow: true,
			expected:      false,
			expectedErr:   "mock store throw",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			req := &oauth.VerifyCallbackReq{
				Provider: oauth.OAuthProviders(test.provider),
				State:    test.state,
			}
			res := &oauth.VerifyCallbackRes{}
			srv := oauthService{
				&google{
					"1234", mockStorer{test.getStateThrow, test.getStateNil, test.getStateMap},
				},
			}

			err := srv.VerifyCallback(context.Background(), req, res)
			if err != nil {
				assert.Equal(t, test.expectedErr, err.Error())
			} else {
				assert.Equal(t, test.expected, res.GetVerified())
			}
		})
	}
}
