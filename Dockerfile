FROM alpine:latest

RUN mkdir /app
WORKDIR /app
COPY ./bin/oauth /app/oauth

CMD ["/app/oauth"]
