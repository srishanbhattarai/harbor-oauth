.DEFAULT_GOAL:= run
.PHONY: proto build run

build:
	- GOOS=linux GOARCH=amd64 go build -o bin/oauth .

proto:
	- protoc -I . --go_out=. --micro_out=. proto/oauth.proto

test:
	- go test -v `go list ./... | grep -v proto`

test_cover:
	- ./test.sh

run:
	- make build
	- docker-compose build --no-cache
	- docker-compose up -d harbor-oauth
