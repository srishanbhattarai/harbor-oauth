// Package physical implements physical storage required for OAuth
package physical

// Storer is an interface for storing OAuth details.
type Storer interface {
	StoreState(uuid string, values map[string]string) error

	GetState(uuid string) (map[string]string, error)
}
