package redis

import (
	"fmt"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
)

// A helper that returns a free TCP port.
func freePort(t *testing.T) (int, error) {
	t.Helper()

	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}

	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}

func TestNew(t *testing.T) {
	p, err := freePort(t)
	if err != nil {
		t.Fatalf("Couldn't get free port: %v", err)
	}

	_, err = New(fmt.Sprintf("localhost:%d", p))
	if err == nil {
		t.Fatalf("Expected an error but got nil")
	}
}

func TestStoreState(t *testing.T) {
	r, err := New("0.0.0.0:60001")
	if err != nil {
		t.Logf("Tests for Redis require the harbor-oauth-redis-test docker container to be running")
		return
	}

	err = r.StoreState("hash", map[string]string{
		"key1": "val1",
		"key2": "val2",
	})

	if err != nil {
		t.Fatalf("Expected no errors, but got: %v", err)
	}

	result, err := r.HGetAll("hash").Result()
	if err != nil {
		t.Fatalf("Expected no errors, but got: %v", err)
	}
	assert.Equal(t, map[string]string{
		"key1": "val1",
		"key2": "val2",
	}, result)
}

func TestGetState(t *testing.T) {
	r, err := New("0.0.0.0:60001")
	if err != nil {
		t.Logf("Tests for Redis require the harbor-oauth-redis-test docker container to be running")
		return
	}

	err = r.StoreState("hash", map[string]string{
		"key1": "val1",
		"key2": "val2",
	})

	if err != nil {
		t.Fatalf("Expected no errors, but got: %v", err)
	}

	result, err := r.GetState("hash")
	if err != nil {
		t.Fatalf("Expected no errors, but got: %v", err)
	}
	assert.Equal(t, map[string]string{
		"key1": "val1",
		"key2": "val2",
	}, result)
}
