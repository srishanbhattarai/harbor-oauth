package redis

import "github.com/go-redis/redis"

// Client represents a connection to the Redis server
type Client struct {
	*redis.Client
}

// New returns a new instance of Redis.
func New(address string) (*Client, error) {
	c := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "",
		DB:       0,
	})

	_, err := c.Ping().Result()

	return &Client{c}, err
}

// StoreState saves a flat hashmap into Redis.
func (c *Client) StoreState(key string, values map[string]string) error {
	for k, v := range values {
		err := c.HSet(key, k, v).Err()
		if err != nil {
			return err
		}
	}

	return nil
}

// GetState retrieves a map for a hash.
func (c *Client) GetState(key string) (map[string]string, error) {
	return c.HGetAll(key).Result()
}
