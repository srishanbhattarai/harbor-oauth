package main

import (
	"context"
	"os"
	"time"

	"github.com/harborapp/harbor-oauth/physical/redis"
	oauth "github.com/harborapp/harbor-oauth/proto"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	log "github.com/sirupsen/logrus"
)

const serviceName = "SERVICE_OAUTH"

func main() {
	r, err := redis.New("harbor-oauth-redis:6379")
	if err != nil {
		log.Fatalf("Could not connect to Redis: ", err)
	}

	log.Infof("Redis Connection Acquired")

	srv := micro.NewService(
		micro.Name(serviceName),
		micro.WrapHandler(logger),
	)

	srv.Init()

	oauthSrv := oauthService{
		googleProvider: &google{
			clientID: os.Getenv("GOOGLE_CLIENT_ID"),
			store:    r,
		},
	}

	oauth.RegisterOAuthHandler(srv.Server(), &oauthSrv)

	if err := srv.Run(); err != nil {
		log.Fatal(err.Error())
	}
}

// logger is a middleware to log request/responses.
func logger(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, rsp interface{}) error {
		start := time.Now()
		log.Infof("[%v] Request: %s", start, req.Method())

		res := fn(ctx, req, rsp)
		elapsed := time.Since(start).Round(time.Millisecond).String()

		log.WithFields(log.Fields{
			"Elapsed": elapsed,
		}).Infof("[%v] Response: %s", res)

		return res
	}
}
