package main

import (
	"context"
	"fmt"

	oauth "github.com/harborapp/harbor-oauth/proto"
	uuid "github.com/hashicorp/go-uuid"
)

type oauthProvider interface {
	LoginURL(
		redirect,
		accessType string,
		scopes []string,
		state map[string]string,
		identifer string,
	) (string, error)

	VerifyCallback(uid string, state map[string]string) (bool, error)
}

// oauthService is the service struct for OAuth.
type oauthService struct {
	googleProvider oauthProvider
}

func (s *oauthService) LoginURL(_ context.Context, req *oauth.LoginURLReq, res *oauth.LoginURLRes) error {
	// Only Google OAuth is supported right now.
	if req.GetProvider() != oauth.OAuthProviders_GOOGLE {
		return fmt.Errorf("Provider unsupported")
	}

	uid, err := uuid.GenerateUUID()
	if err != nil {
		return err
	}

	url, err := s.googleProvider.LoginURL(
		req.GetRedirectURL(),
		req.GetAccessType(),
		req.GetScopes(),
		req.GetState(),
		uid,
	)

	if err != nil {
		return err
	}

	res.Url = url
	res.Identifier = uid

	return nil
}

// VerifyCallback is responsible for validating whether or not the authorization callback is valid or not.
func (s *oauthService) VerifyCallback(_ context.Context, req *oauth.VerifyCallbackReq, res *oauth.VerifyCallbackRes) error {
	// Only Google OAuth is supported right now.
	if req.GetProvider() != oauth.OAuthProviders_GOOGLE {
		return fmt.Errorf("Provider unsupported")
	}

	res.Verified = false
	if ok, err := s.googleProvider.VerifyCallback(req.GetIdentifier(), req.GetState()); ok && err == nil {
		res.Verified = true
	}

	return nil
}
