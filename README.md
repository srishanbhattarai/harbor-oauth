# Harbor OAuth &middot; [![Build Status](https://travis-ci.org/harborapp/harbor-oauth.svg?branch=master)](https://travis-ci.org/harborapp/harbor-oauth)

gRPC based OAuth service with no transport/routing layer bias. It is a small stateful service that handles creation of the login URL, and manages all pieces of state to later verify the callback from the OAuth provider. 

## Running the service
Make sure you have `docker`, and `docker-compose` installed on your machine. 
The `Makefile` provides a recipe `run`, which is the easiest way to bring the stack up.

```
$ make run
```

Also, make sure you have Go installed on your machine. The Docker containers do not use the Go image which would drastically increase the container size, but rather use the small (4-5 MB) Alpine Linux image. The only thing running in the containers are the binaries.

## Tests
Tests rely on the `harbor-oauth-redis-test` container, which isn't created by the `run` recipe. 
```
$ docker-compose up -d harbor-oauth-redis-test
```

Tests can then be run as follows:
```
$ make test
```
